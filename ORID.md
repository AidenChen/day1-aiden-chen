## Objective
- Course introduction.
- Ice-breaking games and course's expectation.
- TED video and discussion.
- Know and build concept map.
- Stand-up meeting.
- ORID.

## Reflective
For me,most of the content is understandable, but some parts of the concept Map 
is little confused.


## Interpretive
I think the most meaningful course for me is `Know and build concept map`.
Because there are many concepts in the computer field or software development
related fields, and these concepts can affect our development results, but 
many concepts are more abstract and complex, after learning this concept map, 
I may try to use this method to understand these concepts.So i think it's good
for me.

## Decisional
Continue to study concept map in my spare time.


